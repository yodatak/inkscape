# SPDX-License-Identifier: GPL-2.0-or-later
set(gimpwidgets_SRC
	ruler.cpp

	# -------
	# Headers
	ruler.h
)

add_inkscape_source("${gimpwidgets_SRC}")
